<?xml version="1.0" encoding="utf-8"?>
<!--
**
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">

    <!-- Status Bar Tuner Items -->
    <string name="statusbar_items_title">Statusbar icon</string>
    <string name="status_bar_vpn">VPN</string>
    <string name="quick_settings_roaming_title">Roaming indicator</string>
    <string name="status_bar_data_saver">Data saver</string>

    <!-- Lockscreen tuner -->
    <string name="lockscreen_tuner_title">Lockscreen shortcuts</string>
    <string name="lockscreen_default">Default</string>
    <string name="lockscreen_hidden">None</string>

    <!-- Power Menu -->
    <string name="global_action_reboot">Restart</string>
    <string name="global_action_reboot_more">Restart\u2026</string>
    <string name="global_action_reboot_sub">System</string>
    <string name="global_action_reboot_recovery">Recovery</string>
    <string name="global_action_reboot_bootloader">Bootloader</string>

    <!-- QS Colums -->
    <string name="qs_menu_item_columns">Columns</string>
    <string name="qs_menu_item_columns_landscape">Columns landscape</string>
    <string name="qs_menu_item_columns_three">3</string>
    <string name="qs_menu_item_columns_four">4</string>
    <string name="qs_menu_item_columns_five">5</string>
    <string name="qs_menu_item_columns_six">6</string>
    <string name="qs_menu_item_columns_seven">7</string>
    <string name="qs_menu_item_columns_eight">8</string>
    <string name="qs_menu_item_columns_nine">9</string>
    <string name="qs_menu_item_reset">Reset</string>
    <string name="qs_menu_item_titles">Show labels</string>
    <string name="qs_menu_item_rows">Rows</string>
    <string name="qs_menu_item_rows_landscape">Rows landscape</string>
    <string name="qs_menu_item_rows_one">1</string>
    <string name="qs_menu_item_rows_two">2</string>
    <string name="qs_menu_item_rows_three">3</string>
    <string name="qs_menu_item_rows_four">4</string>
    <string name="qs_menu_item_qs_columns">Quickbar columns</string>
    <string name="qs_menu_item_columns_auto">Automatic</string>

    <!-- CPUInfo tile -->
    <string name="quick_settings_cpuinfo_label">CPU Info</string>
    <string name="accessibility_quick_settings_cpuinfo_off">CPU Info off</string>
    <string name="accessibility_quick_settings_cpuinfo_on">CPU Info on</string>
    <string name="accessibility_quick_settings_cpuinfo_changed_off">CPU Info turned off</string>
    <string name="accessibility_quick_settings_cpuinfo_changed_on">CPU Info turned on</string>

    <!-- Caffeine tile -->
    <string name="quick_settings_caffeine_label">Caffeine</string>
    <string name="accessibility_quick_settings_caffeine_off">Caffeine off</string>
    <string name="accessibility_quick_settings_caffeine_on">Caffeine on</string>

    <!-- Shows when people have clicked on the custom lockscreen shortcut [CHAR LIMIT=60] -->
    <string name="custom_hint">Swipe from icon to launch</string>

    <!-- Screenshot -->
    <string name="quick_settings_screenshot_label">Screenshot</string>
    <string name="quick_settings_region_screenshot_label">Partial screenshot</string>
    <!-- Partial screenshot -->
    <string name="screenshot_abort_title">No screenshot taken</string>
    <string name="screenshot_abort_text">Operation aborted.</string>

    <!-- Omni weather -->
    <string name="omnijaws_service_error">Error</string>
    <string name="omnijaws_service_unkown">No weather data</string>
    <string name="omnijaws_service_error_long">Error loading weather data</string>
    <string name="omnijaws_service_disabled">Service is disabled</string>
    <string name="omnijaws_detail_header">Weather</string>
    <string name="omnijaws_label_default">Weather</string>
    <string name="omnijaws_current_text">Now</string>
    <string name="omnijaws_package_not_available">OmniJaws not available</string>
    <string name="omnijaws_service_last_update">Last update:</string>

    <!-- Weather string format in expanded statusbar header -->
    <string name="status_bar_weather_format"><xliff:g id="temp">%1$s</xliff:g></string>

    <!-- Location Tile -->
    <string name="accessibility_quick_settings_location_battery_saving">Location reporting: battery saving mode.</string>
    <string name="accessibility_quick_settings_location_gps_only">Location reporting: sensors only mode.</string>
    <string name="accessibility_quick_settings_location_high_accuracy">Location reporting: high accuracy mode.</string>
    <string name="quick_settings_location_battery_saving_label">Battery saving</string>
    <string name="quick_settings_location_battery_saving_label_twoline">Battery\nsaving</string>
    <string name="quick_settings_location_gps_only_label">GPS only</string>
    <string name="quick_settings_location_gps_only_label_twoline">GPS only</string>
    <string name="quick_settings_location_high_accuracy_label">High accuracy</string>
    <string name="quick_settings_location_high_accuracy_label_twoline">High\naccuracy</string>
    <string name="quick_settings_location_detail_title">Location</string>
    <string name="quick_settings_location_detail_menu_title">Select mode:</string>
    <string name="quick_settings_location_detail_mode_high_accuracy_title">High accuracy</string>
    <string name="quick_settings_location_detail_mode_battery_saving_title">Battery saving</string>
    <string name="quick_settings_location_detail_mode_sensors_only_title">Device only</string>
    <string name="quick_settings_location_detail_mode_high_accuracy_description">Use GPS, Wi\u2011Fi, Bluetooth, or cellular networks to determine location </string>
    <string name="quick_settings_location_detail_mode_battery_saving_description">Use Wi\u2011Fi, Bluetooth, or cellular networks to determine location</string>
    <string name="quick_settings_location_detail_mode_sensors_only_description">Use GPS to determine your location</string>

    <!-- VPN QS tile -->
    <string name="quick_settings_vpn_label">VPN</string>
    <string name="quick_settings_vpn_connect_dialog_title">Connect to\u2026</string>
    <string name="vpn_credentials_hint">Please enter your credentials for connecting to <xliff:g id="name">%s</xliff:g></string>
    <string name="vpn_credentials_username">Username</string>
    <string name="vpn_credentials_password">Password</string>
    <string name="vpn_credentials_dialog_connect">Connect</string>

    <!-- QuickSettings: ScreenStabilization tile [CHAR LIMIT=NONE] -->
    <string name="quick_settings_stabilization_label">Screen Stabilization</string>
    <!-- QuickSettings: ScreenStabilization (off) [CHAR LIMIT=NONE] -->
    <string name="quick_settings_stabilization_off">Screen Stabilization</string>
    <!-- QuickSettings: ScreenStabilization (on) [CHAR LIMIT=NONE] -->
    <string name="quick_settings_stabilization_on">Screen Stabilization</string>

    <!-- Compass QS Tile -->
    <string name="quick_settings_compass_label">Compass</string>
    <string name="quick_settings_compass_value"><xliff:g id="degrees">%1$.0f</xliff:g>\u00b0 <xliff:g id="direction">%2$s</xliff:g></string>
    <string name="quick_settings_compass_init">Initializing\u2026</string>
    <string name="accessibility_quick_settings_compass_off">Compass off.</string>
    <string name="accessibility_quick_settings_compass_on">Compass on.</string>
    <string name="accessibility_quick_settings_compass_changed_off">Compass turned off.</string>
    <string name="accessibility_quick_settings_compass_changed_on">Compass turned on.</string>
    <string name="quick_settings_compass_N">N</string>
    <string name="quick_settings_compass_NE">NE</string>
    <string name="quick_settings_compass_E">E</string>
    <string name="quick_settings_compass_SE">SE</string>
    <string name="quick_settings_compass_S">S</string>
    <string name="quick_settings_compass_SW">SW</string>
    <string name="quick_settings_compass_W">W</string>
    <string name="quick_settings_compass_NW">NW</string>

    <!-- VoLTE Icon -->
    <string name="tuner_volte_title">VoLTE</string>

</resources>
